import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Birds from './bird.json'

const TableExample =()=>{
    const sortedBirds = [...Birds].sort((a, b) => a.finnish.localeCompare(b.finnish));
      
    return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead className="table-header">
                    <TableRow>
                        <TableCell style={{ fontWeight: 'bold' }}>Finnish</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }} align="left">Swedish</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }} align="left">English&nbsp;(g)</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }} align="left">Short&nbsp;(g)</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }} align="left">Latin&nbsp;(g)</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {sortedBirds.map((bird) => (
                        <TableRow
                        key={bird.finnish}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                            {bird.finnish}
                        </TableCell>
                        <TableCell align="left">{bird.swedish}</TableCell>
                        <TableCell align="left">{bird.english}</TableCell>
                        <TableCell align="left">{bird.short}</TableCell>
                        <TableCell align="left">{bird.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )
}

export default TableExample;